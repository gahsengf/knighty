using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Knighty.Player
{
    public interface IPlayerState
    {
        void Handle(PlayerController playerController);
    }
}
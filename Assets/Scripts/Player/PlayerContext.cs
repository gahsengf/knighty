using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Knighty.Player
{
    public class PlayerContext
    {
        public IPlayerState CurrentState
        {
            get; set;
        }

        private readonly PlayerController _playerController;

        public PlayerContext(PlayerController playerController) {
            _playerController = playerController;
        }

        public void Transition()
        {
            CurrentState.Handle(_playerController);
        }

        public void Transition(IPlayerState state)
        {
            CurrentState = state;
            CurrentState.Handle(_playerController);
        }
    }
}
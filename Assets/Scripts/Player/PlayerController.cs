using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Knighty.Player
{
    public class PlayerController : MonoBehaviour
    {
        public float PlayerCurrentSpeed { get; set; }

        private IPlayerState _idleState, _moveState, _attackState;
        private PlayerContext _playerContext;

        public Animator playerAnimator;

        void Start()
        {
            playerAnimator = gameObject.GetComponent<Animator>();

            _playerContext = new PlayerContext(this);
            _moveState = gameObject.AddComponent<PlayerMoveState>();
            _idleState = gameObject.AddComponent<PlayerIdleState>();
            _attackState = gameObject.AddComponent<PlayerAttackState>();

            _playerContext.Transition(_idleState);
        }

        public void PlayerMove() {
            _playerContext.Transition(_moveState);
        }

        public void PlayerIdle() {
            _playerContext.Transition(_idleState);
        }

        public void PlayerAttack()
        {
            _playerContext.Transition(_attackState);
        }
    }
}
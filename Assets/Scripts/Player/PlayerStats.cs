using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Knighty.Player
{
    public class PlayerStats
    {
        private float _hitpoints;
        private float _manaPoints;
        private float _baseDamage;
        private float _magicDamage;

        public PlayerStats()
        {
            this._hitpoints = 100f;
            this._manaPoints = 100f;
            this._baseDamage = 20f;
            this._magicDamage = 5f;
        }

        public void SetHP(float hp)
        {
            this._hitpoints = hp;
        }

        public void SetMP(float mp)
        {
            this._manaPoints = mp;
        }

        public void SetBaseDamage(float baseDamage)
        {
            this._baseDamage = baseDamage;
        }

        public void SetMagicDamage(float magicDmg)
        {
            this._magicDamage = magicDmg;
        }
    }
}
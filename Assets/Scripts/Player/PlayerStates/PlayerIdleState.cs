using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Knighty.Player
{
    public class PlayerIdleState : MonoBehaviour, IPlayerState
    {
        private PlayerController _playerController;
        private bool animateDone = false;

        public void Handle(PlayerController playerController)
        {
            if (!_playerController)
                _playerController = playerController;
        }

        public void RemainOnGround()
        {
            _playerController.transform.position = new Vector3(_playerController.transform.position.x, _playerController.transform.position.y, 0);
        }

        private void Update()
        {
            //if (_playerController.playerAnimator.GetBool("isMove").Equals(false) && animateDone == false)
            //{
            //    StartCoroutine(AnimateIdleTimer());
            //}
        }

        private IEnumerator AnimateIdleTimer()
        {
            if (_playerController.playerAnimator.GetBool("isMove").Equals(false))
            {
                yield return new WaitForSeconds(5.0f);
                if (animateDone == false && _playerController.playerAnimator.GetCurrentAnimatorStateInfo(0).IsName("PlayerIdle"))
                {
                    _playerController.playerAnimator.Play("PlayerIdleLookAround");
                    RemainOnGround();

                    yield return new WaitForSeconds(4.0f);

                    _playerController.playerAnimator.Play("PlayerIdle");
                    animateDone = true;

                    yield return new WaitForSeconds(5.0f);
                    animateDone = false;
                }
            }
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Knighty.Player
{
    public class PlayerAttackState : MonoBehaviour, IPlayerState
    {
        private PlayerController _playerController;

        public void Handle(PlayerController playerController)
        {
            if (!_playerController)
                _playerController = playerController;

        }

        private void Update()
        {
            if(_playerController)
            {
                if(IsMouseKeyDown())
                {
                    _playerController.playerAnimator.SetBool("isAttack", true);
                }
                else
                {
                    _playerController.playerAnimator.SetBool("isAttack", false);
                }
            }
        }

        private bool IsMouseKeyDown()
        {
            bool isMouseKey0Down = Input.GetMouseButtonDown(0);

            if (isMouseKey0Down)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
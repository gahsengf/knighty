using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Knighty.Player {
    public class PlayerMoveState : MonoBehaviour, IPlayerState
    {
        private PlayerController _playerController;
        
        public void Handle(PlayerController playerController)
        {
            if (!_playerController)
                _playerController = playerController;

            SlowMovement();
        }

        private void Update()
        {
            bool isUpKey = Input.GetKey(KeyCode.W);
            bool isDownKey = Input.GetKey(KeyCode.S);
            bool isLeftKey = Input.GetKey(KeyCode.A);
            bool isRightKey = Input.GetKey(KeyCode.D);
            bool isRunKey = Input.GetKey(KeyCode.Space);

            if (_playerController)
            {
                // Animate Player Movement
                //if (isUpKey || isDownKey || isLeftKey || isRightKey)
                //    _playerController.playerAnimator.SetBool("isWalk", true);
                //else
                //    _playerController.playerAnimator.SetBool("isWalk", false);

                if(IsKeyDown() && isRunKey)
                {
                    FastMovement();

                    _playerController.playerAnimator.SetBool("isIdle", false);
                    _playerController.playerAnimator.SetBool("isWalk", false);
                    _playerController.playerAnimator.SetBool("isRun", true);
                }
                else if (IsKeyDown())
                {
                    SlowMovement();

                    _playerController.playerAnimator.SetBool("isIdle", false);
                    _playerController.playerAnimator.SetBool("isWalk", true);
                    _playerController.playerAnimator.SetBool("isRun", false);
                }
                else
                {
                    _playerController.playerAnimator.SetBool("isIdle", true);
                    _playerController.playerAnimator.SetBool("isWalk", false);
                    _playerController.playerAnimator.SetBool("isRun", false);
                }

                MoveDirection(isUpKey, isDownKey, isLeftKey, isRightKey);
            }
        }

        private void MoveDirection(bool isUpKey, bool isDownKey, bool isLeftKey, bool isRightKey)
        {
            if(_playerController.playerAnimator.GetCurrentAnimatorStateInfo(0).IsName("PlayerIdleLookAround"))
            {
                // If player idle and look around animation played
                // Player cannot move
                _playerController.PlayerIdle();
            }
            //else if (isLeftKey && isUpKey)
            //{
            //    // Move Up Left
            //    _playerController.transform.Translate(new Vector3(1, 1, 0) * (_playerController.PlayerCurrentSpeed * Time.deltaTime));
            //    FaceLeft();
            //}
            //else if (isLeftKey && isDownKey)
            //{
            //    // Move Up Left
            //    _playerController.transform.Translate(new Vector3(1, -1, 0) * (_playerController.PlayerCurrentSpeed * Time.deltaTime));
            //    FaceLeft();
            //}
            //else if (isRightKey && isUpKey)
            //{
            //    // Move Up Right
            //    _playerController.transform.Translate(new Vector3(1, 1, 0) * (_playerController.PlayerCurrentSpeed * Time.deltaTime));
            //    FaceRight();
            //}
            //else if (isRightKey && isDownKey)
            //{
            //    // Move Up Left
            //    _playerController.transform.Translate(new Vector3(1, -1, 0) * (_playerController.PlayerCurrentSpeed * Time.deltaTime));
            //    FaceRight();
            //}
            else if (isLeftKey)
            {
                // Move Left
                _playerController.transform.Translate(Vector2.right * (_playerController.PlayerCurrentSpeed * Time.deltaTime));
                _playerController.transform.rotation = Quaternion.Euler(0, -180f, 0);
            }
            else if (isRightKey)
            {
                // Move Right
                _playerController.transform.Translate(Vector2.right * (_playerController.PlayerCurrentSpeed * Time.deltaTime));
                _playerController.transform.rotation = Quaternion.Euler(0, 0, 0);
            }
            //else if (isUpKey)
            //{
            //    // Move Up
            //    _playerController.transform.Translate(Vector2.up * (_playerController.PlayerCurrentSpeed * Time.deltaTime));
            //    FaceLeftOrRight();
            //}
            //else if (isDownKey)
            //{
            //    // Move Down
            //    _playerController.transform.Translate(Vector2.down * (_playerController.PlayerCurrentSpeed * Time.deltaTime));
            //    FaceLeftOrRight();
            //}
            
        }

        private bool IsKeyDown()
        {
            bool isUpKey = Input.GetKey(KeyCode.W);
            bool isDownKey = Input.GetKey(KeyCode.S);
            bool isLeftKey = Input.GetKey(KeyCode.A);
            bool isRightKey = Input.GetKey(KeyCode.D);

            if (isLeftKey || isRightKey)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void FaceLeftOrRight()
        {
            if (_playerController.transform.rotation.y.Equals(0))
                _playerController.transform.rotation = Quaternion.Euler(0, 0, 0);
            else
                _playerController.transform.rotation = Quaternion.Euler(0, -180, 0);
        }

        private void FaceRight()
        {
            _playerController.transform.rotation = Quaternion.Euler(0, 0, 0);
        }

        private void FaceLeft()
        {
            _playerController.transform.rotation = Quaternion.Euler(0, -180, 0);
        }

        private void SlowMovement()
        {
            _playerController.PlayerCurrentSpeed = 3.0f;
        }

        private void FastMovement()
        {
            _playerController.PlayerCurrentSpeed = 6.0f;
        }
    }
}
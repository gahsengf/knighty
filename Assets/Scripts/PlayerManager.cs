using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Knighty.Player;

public class PlayerManager : MonoBehaviour
{
    private PlayerController _playerController;

    void Start()
    {
        _playerController = (PlayerController)FindObjectOfType(typeof(PlayerController));
    }

    void Update()
    {
        if(Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.W))
        {
            _playerController.PlayerMove();
        }
        else if(Input.GetMouseButtonDown(0))
        {
            _playerController.PlayerAttack();
        }
        else
        {
            _playerController.PlayerIdle();
        }
    }
}
